# WisdomBookSystem_Django

#### 介绍
本项目是作者的本科毕业设计项目--智慧图书借阅系统.后台使用python语言开发,前端使用Vue.js开发.可以解释为是一个电商项目,但是处于时间和成本原因做的比较着急, 在完成度上差不多75%,剩下的功能看心情吧.

#### 软件架构
软件架构说明
python
Django
Django REST framework

Vue.js
Node.js--Liveserver

Mysql
Redis

Docker
FastDFS
Elasticsearch

celery
crontab

#### 安装教程

1. 使用请保证网络连接正常,本项目使用的数据库和文件服务器均为云服务.
2. 运行Django服务
3. 运行前端服务器liveserver
4. 启动celery异步任务进程
5. 添加crontab定时任务

#### 使用说明

1. xxxx
2. xxxx
3. xxxx



