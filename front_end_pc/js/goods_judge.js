var vm = new Vue({
    el: '#app',
    data: {
        host,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        order_id: '',
        books: []
    },
    mounted: function(){
        this.order_id = this.get_query_string('order_id');
        axios.get(this.host+'/orders/'+this.order_id+'/uncommentgoods/', {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json'
            })
            .then(response => {
                this.books = response.data;
                for(var i=0;i<this.books.length;i++){
                    this.books[i].url = '/books/' + this.books[i].id + '.html';
                    Vue.set(this.books[i], 'score', 0); // 记录随鼠标变动的星星数
                    Vue.set(this.books[i], 'display_score', 0); // 展示变动的分数值
                    this.books[i].final_score = 0; // 记录用户确定的星星数
                    Vue.set(this.books[i], 'comment', '');
                    Vue.set(this.books[i], 'is_anonymous', false);
                }
            })
            .catch(error => {
                console.log(error.response.data);
            })
    },
    methods: {
        // 退出
        logout: function(){
            sessionStorage.clear();
            localStorage.clear();
            location.href = '/login.html';
        },
        // 获取url路径参数
        get_query_string: function(name){
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        on_stars_mouseover: function(index, score){
            this.books[index].score = score;
            this.books[index].display_score = score * 20;
        },
        on_stars_mouseout: function(index) {
            this.books[index].score = this.books[index].final_score;
            this.books[index].display_score = this.books[index].final_score * 20;
        },
        on_stars_click: function(index, score) {
            this.books[index].final_score = score;
        },
        save_comment: function(index){
            var book = this.books[index];
            if (book.comment.length < 5){
                alert('请填写多余5个字的评价内容');
            } else {
                axios.post(this.host+'/orders/'+this.order_id+'/comments/', {
                        order: this.order_id,
                        book: book.book.id,
                        comment: book.comment,
                        score: book.final_score,
                        is_anonymous: book.is_anonymous,
                    }, {
                        headers: {
                            'Authorization': 'JWT ' + this.token
                        },
                        responseType: 'json'
                    })
                    .then(response => {
                        this.books.splice(index, 1);
                    })
                    .catch(error => {
                        console.log(error.response.data);
                    })
            }
        }
    }
});