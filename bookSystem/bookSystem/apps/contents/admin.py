from django.contrib import admin

# Register your models here.
from contents.models import ContentCategory, Content
from utils.BaseAdmin import BaseAdmin


class ContentCategoryAdmin(BaseAdmin):

    list_display = ['id', 'name', 'key']


class ContentAdmin(BaseAdmin):
    list_display = ['id', 'category', 'title', 'url','status']


admin.site.register(ContentCategory,ContentCategoryAdmin)
admin.site.register(Content, ContentAdmin)
