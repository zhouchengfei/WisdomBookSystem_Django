# -*-coding:utf-8-*-
from haystack import indexes

from books.models import Books


class BookIndex(indexes.SearchField):
    """Book索引数据模型"""
    text = indexes.CharField(document=True, use_template=True)


    def get_model(self):
        """
        返回建立索引的数据模型
        :return:
        """
        return Books

    def index_queryset(self, using=None):
        """
        返回要建立索引查询集
        :return:
        """
        return self.get_model().objects.filter(is_launched=True)
