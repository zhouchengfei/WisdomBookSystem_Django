from django.contrib import admin

# Register your models here.
from books.models import BooksCategory, BooksChannel, Books
from utils.BaseAdmin import BaseAdmin


class BookInfoAdmin(BaseAdmin):
    """站点图书管理"""

    list_display = ['id', 'name', 'author', 'brand', 'pub_date','surplus']

    def save_model(self, request, obj, form, change):
        """
        保存数据
        :param request:
        :param obj:
        :param form:
        :param change:
        :return:
        """
        obj.save()
        from celery_tasks.html.tasks import generate_static_book_detail_html
        generate_static_book_detail_html.delay(obj.id)



class BooksCategoryAdmin(admin.ModelAdmin):
    """图书类别"""
    list_display = ['id', 'name', 'parent']


    def save_model(self, request, obj, form, change):
        obj.save()
        from celery_tasks.html.tasks import generate_static_list_search_html,generate_static_index_html
        generate_static_list_search_html.delay()
        generate_static_index_html()


    def delete_model(self, request, obj):
        obj.delete()
        from celery_tasks.html.tasks import generate_static_list_search_html,generate_static_index_html
        generate_static_index_html()
        generate_static_list_search_html.delay()



admin.site.register(BooksCategory, BooksCategoryAdmin)
admin.site.register(BooksChannel)
admin.site.register(Books,BookInfoAdmin)


