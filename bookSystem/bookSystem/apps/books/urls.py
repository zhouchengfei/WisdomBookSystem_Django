# -*-coding:utf-8-*-
from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from books import views

urlpatterns = [
    url(r"^categories/(?P<pk>\d+)/skus/$",views.BooksListView.as_view()),
    url(r"^categories/(?P<pk>\d+)/$", views.CategoriesView.as_view()),  # 面包屑导航
    url(r'^browse_histories/$', views.BookHistoryView.as_view()),
    url(r"^books/(?P<pk>\d+)/comments/$", views.CommentShowView.as_view()),


]

router = DefaultRouter()
router.register('skus/search', views.BookSerchViewSet, base_name="skus_search")
urlpatterns += router.urls