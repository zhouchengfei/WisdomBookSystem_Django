# Generated by Django 2.1.7 on 2019-03-28 14:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0013_auto_20190218_1738'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='books',
            name='category1',
        ),
        migrations.RemoveField(
            model_name='books',
            name='category2',
        ),
    ]
