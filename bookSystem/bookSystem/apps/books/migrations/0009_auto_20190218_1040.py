# Generated by Django 2.1.7 on 2019-02-18 02:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0008_books_comments'),
    ]

    operations = [
        migrations.RenameField(
            model_name='books',
            old_name='category1_id',
            new_name='category1',
        ),
        migrations.RenameField(
            model_name='books',
            old_name='category2_id',
            new_name='category2',
        ),
        migrations.RenameField(
            model_name='books',
            old_name='category3_id',
            new_name='category3',
        ),
    ]
