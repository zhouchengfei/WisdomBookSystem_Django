from django.conf import settings
from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.
from utils.models import BaseModel


class BooksCategory(BaseModel):
    """
    图书类别
    """
    name = models.CharField(max_length=10, verbose_name='名称')
    parent = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE, verbose_name='父类别')

    class Meta:
        db_table = 'tb_books_category'
        verbose_name = '图书类别'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

    admin_order_field = "parent"


class BooksChannel(BaseModel):
    """
    图书频道
    """
    group_id = models.IntegerField(verbose_name='组号')
    category = models.ForeignKey(BooksCategory, on_delete=models.CASCADE, verbose_name='顶级图书类别')
    url = models.CharField(max_length=50, verbose_name='频道页面链接')
    sequence = models.IntegerField(verbose_name='组内顺序')

    class Meta:
        db_table = 'tb_books_channel'
        verbose_name = '图书频道'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.category.name

class Books(BaseModel):
    """图书info"""
    isbn = models.CharField(max_length=13, verbose_name='ISBN')
    name = models.CharField(max_length=50, verbose_name='书名')
    author = models.CharField(max_length=50, verbose_name='作者')
    translator = models.CharField(max_length=50, blank=True, verbose_name='翻译者')
    brand = models.CharField(max_length=50, verbose_name='出版社')
    public_date = models.DateTimeField(verbose_name='出版时间')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='每月计费')
    purchase_price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='书价')
    scorce = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='评分')
    is_launched = models.BooleanField(default=True, verbose_name='是否上架')
    comments = models.IntegerField(default=0, verbose_name='评价数')
    category3 = models.ForeignKey(BooksCategory, on_delete=models.PROTECT, related_name='cat3_books', verbose_name='三级类别')
    total = models.IntegerField(default=0, verbose_name='总库存')
    borrow = models.IntegerField(default=0, verbose_name='出借')
    # image_url = models.CharField(max_length=200, default='', null=True, blank=True, verbose_name='默认图片')
    image_url = models.ImageField(null=True, blank=True, verbose_name='默认图片')

    introduce = RichTextUploadingField(default='', verbose_name='图书介绍') # 图书介绍
    desc_author = RichTextUploadingField(default='', verbose_name='作者介绍')   # 作者介绍
    desc_atalog = RichTextUploadingField(default='', verbose_name='目录')  # 借阅服务


    class Meta:
        db_table = 'tb_books'
        verbose_name = '图书信息'
        verbose_name_plural = verbose_name

    def pub_date(self):
        return self.public_date.strftime('%Y年%m月%d日')
    pub_date.short_description = '出版日期'

    def surplus(self):
        """admin显示剩余量"""
        return (self.total - self.borrow)
    surplus.short_description = '剩余数量'


    def __str__(self):
        return '%s: %s' % (self.id, self.name)


