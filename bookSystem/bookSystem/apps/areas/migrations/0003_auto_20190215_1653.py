# Generated by Django 2.1.7 on 2019-02-15 08:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('areas', '0002_auto_20190215_1422'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='create_time',
            field=models.DateTimeField(auto_now_add=True, verbose_name='创建时间'),
        ),
        migrations.AlterField(
            model_name='address',
            name='update_time',
            field=models.DateTimeField(auto_now=True, verbose_name='更新时间'),
        ),
    ]
