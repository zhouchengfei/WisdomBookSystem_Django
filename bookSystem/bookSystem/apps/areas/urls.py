# -*-coding:utf-8-*-
from django.conf.urls import url

from areas import views

urlpatterns = [
    url(r"^areas/$",views.ProvinceView.as_view()),
    url(r"^areas/(?P<id>\d+)/$",views.AreasView.as_view()),
    url(r"^addresses/$", views.AddressesView().as_view()),
    url(r"^addresses/(?P<pk>\d+)/$",views.AddressesView().as_view()),
    url(r"^addresses/(?P<pk>\d+)/title/$", views.AddressesChangeTitleView.as_view()),

]