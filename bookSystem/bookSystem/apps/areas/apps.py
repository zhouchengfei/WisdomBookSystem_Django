from django.apps import AppConfig


class AreasConfig(AppConfig):
    name = 'areas'
    verbose_name = '地址管理'
