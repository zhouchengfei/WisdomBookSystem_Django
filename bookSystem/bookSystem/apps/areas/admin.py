from django.contrib import admin

# Register your models here.
from areas.models import Address
from utils.BaseAdmin import BaseAdmin


class AddressAdmin(BaseAdmin):
    """收件地址"""
    list_display = ['id', 'user','title']


admin.site.register(Address, AddressAdmin)
