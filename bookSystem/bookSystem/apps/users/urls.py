# -*-coding:utf-8-*-
from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token

from users import views

urlpatterns = [
    url(r'^usernames/(?P<username>\w{5,20})/count/$', views.UsernameCountView.as_view()),
    url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view()),
    url(r"^users/$", views.UserView.as_view()),
    # url(r'^authorizations/$', obtain_jwt_token),
    url(r'^authorizations/$',views.UserAuthorizeView.as_view()),
    url(r'^user/$', views.UserDetailView.as_view()),
    url(r'^email/$', views.EmailView.as_view()),  # 设置邮箱
    url(r'^email/verification/$', views.VerifyEmailView.as_view()), # 验证邮箱
    url(r'^users/(?P<pk>\d+)/password/', views.UpdatePasswdView.as_view()),    # 修改密码
    url(r'^accounts/(?P<name>\w+)/sms/token/$', views.VerificationAccountView.as_view()), # 找回密码一:验证用户
    url(r'^accounts/(?P<name>\w+)/password/token/$', views.VerificationSMSView.as_view()), # 找回密码二:验证短信


]