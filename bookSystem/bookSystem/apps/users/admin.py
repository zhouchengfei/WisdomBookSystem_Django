from django.contrib import admin
from users.models import User
# Register your models here.
from utils.BaseAdmin import BaseAdmin


class UserAdmin(BaseAdmin):
    """用户数据显示"""

    list_display = ['id','username', 'email', 'mobile', 'date_joined']

admin.site.register(User,UserAdmin)

