from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


class User(AbstractUser,):
    """用户模型类"""
    mobile = models.CharField(max_length=11, unique=True, verbose_name='手机号')   # 手机号即借书卡号
    email_active = models.BooleanField(default=False, verbose_name='邮箱验证状态')
    # default_address = models.ForeignKey('Address', related_name='users', null=True, blank=True,
    #                                     on_delete=models.SET_NULL, verbose_name='默认地址')


    class Meta:
        db_table = 'tb_users'
        verbose_name = '用户'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '%s: %s' % (self.id, self.username)

