from django.db import models

# Create your models here.
from bookSystem.utils.models import BaseModel


class OAuthUser(BaseModel):
    """
    QQ登录用户数据
    """
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, verbose_name='用户')
    openid = models.CharField(max_length=64, verbose_name='QQ登录id', db_index=True)  # QQ用户标识openid
    weibotoken = models.CharField(max_length=64, verbose_name='新浪微博id', db_index=True)  # 微博用户登录Access_token
    wechat = models.CharField(max_length=64, verbose_name='微信账号登录id', db_index=True)    # 微信登录的识别码

    class Meta:
        db_table = 'tb_oauth'
        verbose_name = '第三方登录账号'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '%s: %s' % (self.id, self.user)
