from django.apps import AppConfig


class OauthConfig(AppConfig):
    name = 'oauth'
    verbose_name = '第三方登录'
