from django.contrib import admin

# Register your models here.
from oauth.models import OAuthUser

admin.site.register(OAuthUser)
