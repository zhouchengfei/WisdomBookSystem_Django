# -*-coding:utf-8-*-
from django.conf.urls import url

from oauth import views

urlpatterns = [
    url(r"^oauth/qq/authorization/$", views.QQAuthURLLView.as_view()),
    url(r"^oauth/qq/user/$", views.QQOauthView.as_view()),
    url(r"^oauth/weibo/authorization/", views.WeiboAuthURLLView.as_view()),
    url(r"^oauth/sina/user/$", views.WeiboOauthView.as_view()),
]