# Generated by Django 2.1.7 on 2019-02-20 10:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='orderbooks',
            name='count',
        ),
        migrations.AddField(
            model_name='orderbooks',
            name='duration',
            field=models.IntegerField(default=1, verbose_name='借阅时长/月'),
        ),
        migrations.AlterField(
            model_name='orderbooks',
            name='book',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='books.Books', verbose_name='订单图书'),
        ),
    ]
