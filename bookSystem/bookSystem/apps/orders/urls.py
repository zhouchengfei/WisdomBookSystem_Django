# -*-coding:utf-8-*-
from django.conf.urls import url

from orders import views

urlpatterns = [
    url(r'^orders/settlement/$', views.OrderSettlementView.as_view()),
    url(r'^saveorders/$', views.SaveOrderView.as_view()),
    url(r'^orders/list/$', views.OrderListView.as_view()),
    url(r'^orders/(?P<order_id>\w+)/received/$', views.OrderReceivedView.as_view()),
    url(r'^orders/(?P<pk>\w+)/uncommentgoods/$', views.OrderUncommentedView.as_view()),
    url(r'^orders/(?P<order_id>\w+)/comments/', views.CommentSaveView.as_view()),
]