from django.contrib import admin

# Register your models here.
from orders.models import OrderInfo, OrderBooks



admin.site.register(OrderInfo)
admin.site.register(OrderBooks)