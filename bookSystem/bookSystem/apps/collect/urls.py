# -*-coding:utf-8-*-
from django.conf.urls import url

from collect import views

urlpatterns = [
    url(r"^cart/$", views.CollectView.as_view()),
    url(r"^cart/selection/$", views.CollectSelectView.as_view()),


]