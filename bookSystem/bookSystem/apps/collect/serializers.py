# -*-coding:utf-8-*-
from rest_framework import serializers

from books.models import Books


class CollectSerializer(serializers.Serializer):
    """
    我的收藏序列化器
    """
    book_id = serializers.IntegerField(label="图书id", min_value=1)
    duration = serializers.IntegerField(label="借阅时长(月)", min_value=1,default=2)
    selected = serializers.BooleanField(label='是否勾选', default=True)

    def validate(self, data):
        try:
            sku = Books.objects.get(id=data['book_id'])
        except Books.DoesNotExist:
            raise serializers.ValidationError('图书不存在')
        return data


class CollectBookSerializer(serializers.ModelSerializer):
    """
    我的收藏图书数据序列化器
    """
    duration = serializers.IntegerField(label="借阅时长(月)")
    selected = serializers.BooleanField(label='是否勾选')

    class Meta:
        model = Books
        fields = ('id', 'duration','selected','name','price', 'image_url')


class CollectDeleteSerializer(serializers.Serializer):
    """删除收藏"""
    book_id = serializers.IntegerField(label="图书id", min_value=1)

    def validate_book_id(self, value):
        try:
            book = Books.objects.get(id=value)
        except Books.DoesNotExit:
            raise serializers.ValidationError('图书不存在')

        return value

class CollectSelectAllSerializer(serializers.Serializer):
    """我的收藏全选/取消全选"""
    selected = serializers.BooleanField(label="全选")




