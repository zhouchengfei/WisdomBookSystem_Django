from django.apps import AppConfig


class CollectConfig(AppConfig):
    name = 'collect'
    verbose_name = '图书收藏'
