# -*-coding:utf-8-*-
from django.contrib import admin
class BaseAdmin(admin.ModelAdmin):
    """站点基本配置"""
    list_per_page = 20  # 显示数量

    actions_on_top = True   # 在顶部不在底部显示操作选项
    actions_on_bottom = False

    admin_order_field = "create_time"    # 按照创建时间排序