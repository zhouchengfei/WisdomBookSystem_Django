# -*-coding:utf-8-*-
from django.conf import settings
from django.core.mail import send_mail

from celery_tasks.main import app


@app.task(name='send_verify_email')
def send_verify_email(to_email, verify_url):
    """
    发送验证邮箱邮件
    :param to_email: 收件人邮箱
    :param verify_url: 验证链接
    :return: None
    """
    subject = "智慧图书馆邮箱验证"
    html_message = '<p>尊敬的读者您好！</p>' \
                   '<p>感谢您使用智慧图书馆。</p>' \
                   '<p>您的邮箱为：%s 。请点击此链接激活您的邮箱：</p>' \
                   '<p><a href="%s">%s<a></p>' % (to_email, verify_url, verify_url)
    send_mail(subject, "", settings.EMAIL_FROM, [to_email], html_message=html_message)


@app.task(name='lending_timeout')
def lending_timeout(to_email, html_message):
    """
    提醒还书邮件
    :param to_email:
    :param html_message:
    :return:
    """
    subject = "智慧图书馆借阅提醒"
    send_mail(subject, "", settings.EMAIL_FROM, [to_email], html_message=html_message)

